import api.APIInterface;
import api.request.TestRequest;
import api.response.TestResponse;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class Samples {

    private static Retrofit retrofit;
    private static APIInterface api;

    public static void main(String... args) throws Exception {
        retrofit = new Retrofit.Builder()
                .baseUrl("http://41.206.29.44:8080/TeasyAppManager/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        api = retrofit.create(APIInterface.class);

        /**
         * GET request sample
         * */
        Response<TestResponse> response = api.testRequest1(100, "qwerty").execute();
        if(response.code() == 200) {    //http code
            TestResponse responseModel = response.body();
            System.out.println(responseModel.message);
        }

        /**
         * POST request sample
         * */
        response = api.testRequest2(100, "qwerty").execute();
        if(response.code() == 200) {
            TestResponse responseModel = response.body();
            System.out.println(responseModel.message);
        }

        /**
         * POST JSON
         * */
        TestRequest request = new TestRequest(100, "qwerty");
        response = api.testRequest3("test", request).execute();
        if(response.code() == 200) {
            TestResponse responseModel = response.body();

            //Backend doesn't support JSON, so that it will return error
            System.out.println(responseModel.message);
        }
    }

}
