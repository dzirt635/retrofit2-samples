package api;

import api.request.TestRequest;
import api.response.TestResponse;
import retrofit2.Call;
import retrofit2.http.*;

public interface APIInterface {

    @GET("api/test?responseFormat=JSON")
    Call<TestResponse> testRequest1(
            @Query("amount") long amount,
            @Query("param") String param
    );

    @FormUrlEncoded
    @POST("api/test?responseFormat=JSON")
    Call<TestResponse> testRequest2(
            @Field("amount") long amount,
            @Field("param") String param
    );

    @POST("api/{method_name}?responseFormat=JSON")
    Call<TestResponse> testRequest3(
            @Path("method_name") String methodName,
            @Body TestRequest request
    );

}
