package api.request;

public class TestRequest {

    public long amount;
    public String param;

    public TestRequest(long amount, String param) {
        this.amount = amount;
        this.param = param;
    }

}
